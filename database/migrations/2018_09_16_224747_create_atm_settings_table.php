<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtmSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atm_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('note_20');
            $table->integer('note_50');
            $table->integer('note_100');
            $table->integer('note_500');
            $table->integer('note_1000');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atm_settings');
    }
}
