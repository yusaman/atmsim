<?php

use Illuminate\Database\Seeder;
use App\ATMSettings;

class ATMSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ATMSettings::create([
            'note_20' => 100,
            'note_50' => 100,
            'note_100' => 100,
            'note_500' => 100,
            'note_1000' => 100
        ]);
    }
}
