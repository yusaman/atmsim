<?php

use Illuminate\Database\Seeder;
use App\Accounts;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Accounts::create([
            'account_name' => 'Default Account',
            'withdraw' => 00,
            'balance' => 10000,
        ]);
    }
}
