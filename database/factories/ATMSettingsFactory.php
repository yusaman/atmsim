<?php

use Faker\Generator as Faker;

$factory->define(App\ATMSettings::class, function (Faker $faker) {
    return [
        'note_20' => 10,
        'note_50' => 10,
        'note_100' => 10,
        'note_500' => 10,
        'note_1000' => 10,
    ];
});
