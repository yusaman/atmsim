<?php

use Faker\Generator as Faker;

$factory->define(App\Accounts::class, function (Faker $faker) {
    return [
        'account_name' => 'Default Account',
        'balance' => 9999,
    ];
});
