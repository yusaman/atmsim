<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\ATM;
use App\Accounts;
use App\ATMSettings;

class HomeController extends Controller
{
	private $atm;
	public function __construct(ATM $atm)
	{
		$this->atm = $atm;
		$account = Accounts::first();
    	$settings = ATMSettings::first();

    	$this->atm->init($settings, $account);
	}
	
    public function index(Request $request)
    {
    	$bankNotes = $this->atm->getAllBankNotes();
    	$data['bankNotes'] = [];
    	foreach($bankNotes as $key => $value )
    	{
    		$this->atm->setBankNote($value, 20);
    		$data['bankNotes']['note_'.$value] = 20;
    	}

    	$data['total'] = array_sum($bankNotes);

    	$this->atm->setBalance(10000);

    	$data['balance'] = $this->atm->getBalance();

    	return view('home')->with($data);;
    }

    public function home(Request $request)
    {
    	$data = $this->_getBaseData();

		if( $request->has('withdrawal') )
    	{
    		$amount = $request->input('withdrawal');
    		$notes = $this->atm->dispense($amount);

    		$data = $this->_getBaseData();

    		$data['toWithdraw'] = $amount;
    		$data['returnNotes'] = $notes;
    	}

    	return view('home')->with($data);;
    }

    public function settings(Request $request)
    {
    	if( $request->has('bankNotes') )
    	{
	    	foreach($request->input('bankNotes') as $key => $value)
	    	{
	    		$_default = 0;
	    		$_note = str_replace('note_', '', $key);
	    		if( is_null($value) || empty($value) || !is_numeric($value) )
	    		{
	    			$this->atm->setBankNote($_note, $_default);
	    		}
	    		else
	    		{
	    			$this->atm->setBankNote($_note, $value);
	    		}
	    	}
	    }

    	if( $request->has('initial_balance') )
    	{
    		$this->atm->setBalance($request->input('initial_balance'));
    	}

    	return redirect()->to('home');

    }

    public function _getBaseData()
    {
    	$bankNotes = $this->atm->getAllBankNotesAmount();
    	$bankNotesValue = $this->atm->getAllBankNotesWithValue();

    	$data['bankNotes'] = $bankNotes;
    	$data['bankNotesValue'] = $bankNotesValue;

    	$data['total'] = array_sum($bankNotesValue);

    	$data['balance'] = $this->atm->getBalance();

    	return $data;
    }

    public function pr($a)
    {
    	echo '<pre>';
    	print_r($a);
    	echo '</pre>';
    }
}
