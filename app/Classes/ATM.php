<?php
namespace App\Classes;

use App\ATMSettings;
use App\Accounts;

class ATM
{
	private $AllBankNotes = [20, 50, 100, 500, 1000];
	private $note_20;
	private $note_50;
	private $note_100;
	private $note_500;
	private $note_1000;
	private $total = 0;
	private $toReturn;
	private $skipNote;

	private $balance;

	private $accounts;
	private $settings;

	public function init(ATMSettings $settings, Accounts $account)
	{
		$this->settings = $settings;
		$this->accounts = $account;
	}

	public function getBalance()
	{
		$balance = $this->balance = $this->accounts->balance;
		return $balance;
	}

	public function setBalance($balance = null)
	{
		$_balance = 0;
		if( is_numeric($balance) )
		{
			$_balance = $balance;
			$this->accounts->balance = $balance;
			$this->accounts->save();
		}

		return $this->balance = $balance;
	}

	public function setBankNote($noteType = null, $amount = 0)
	{
		if( !in_array($noteType, $this->getAllBankNotes()) )
		{
			return false;
		}

		if( !is_numeric($amount) || $amount <= 0 )
		{
			$amount = 0;
		}

		$_note = "note_{$noteType}";
		$this->$_note = $amount;

		$this->settings->$_note = $amount;
		$this->settings->save();

		return $this->$_note;
	}

	public function getBankNote($noteType = null)
	{
		if( !in_array($noteType, $this->getAllBankNotes()) )
		{
			return false;
		}

		$_note = "note_{$noteType}";
		return $this->settings->$_note;
	}

	public function getAllBankNotes()
	{
		return $this->AllBankNotes;
	}

	public function getAllBankNotesAmount()
	{
		$_all = $this->getAllBankNotes();
		$_allBankNotes = [];
		foreach( $_all as $key => $value )
		{
			$_note = "note_{$value}";
			$_allBankNotes['note_'.$value] = $this->settings->$_note;
		}
		return $_allBankNotes;
	}

	public function getAllBankNotesWithValue()
	{
		$_all = $this->getAllBankNotes();
		$_allBankNotes = [];
		foreach( $_all as $key => $value )
		{
			$_allBankNotes['note_'.$value] = $this->getBankNoteValue($value);
		}

		return $_allBankNotes;
	}

	public function getBankNoteValue($noteType = null)
	{
		if( !in_array($noteType, $this->getAllBankNotes()) )
		{
			return false;
		}
		$_note = "note_{$noteType}";
		$val = $this->settings->$_note * $noteType;
		return $val;
	}

	public function getTotalAmount()
	{
		$total = 0;
		$all = $this->getAllBankNotesWithValue();

		return array_sum($all);
	}

	public function dispense($amount = 0)
	{
		$somePossibleNotes = $this->_dispense($amount);

		if( $somePossibleNotes !== false )
		{
			$this->setBalance( $this->balance - $amount );
			foreach($somePossibleNotes as $noteType => $noteCount )
			{
				if( $noteCount > 0 )
				{
					$noteType = str_replace('note_', '', $noteType);
					$this->setBankNote($noteType, $this->getBankNote($noteType) - $noteCount );
				}
			}
			return $somePossibleNotes;	
		}
		else
		{
			return false;
		}

	}

	public function _dispense($amount = 0, $secondRound = false)
	{
		if( $amount < 20 )
		{
			return false;
		}

		if( !is_numeric($amount) )
		{
			return false;
		}

		$total = $this->getTotalAmount();
		if( $amount > $total )
		{
			return false;
		}

		$balance = $this->getBalance();
		if( $amount > $balance )
		{
			return false;
		}

		$allBankNotesWithValue = $this->getAllBankNotesWithValue();
		$availableBankNotes = array_filter($allBankNotesWithValue, function($value){
			return $value > 0;
		});

		if( !$secondRound )
		{
			$returnBankNotes = array_map(function($value){
				return 0;
			}, $allBankNotesWithValue);

			$this->toReturn = $returnBankNotes;
		}

		$_second = 0;
		switch($amount)
		{
			case $amount >= 1000: 
				$_first = floor($amount / 1000);
				$_second = $amount % 1000;
				$this->toReturn['note_1000'] = (int)$_first;
				break;
			case $amount >= 500;
				$_first = floor($amount / 500);
				$_second = $amount % 500;
				$this->toReturn['note_500'] = (int)$_first;
				break;
			case $amount >= 100;
				$_first = floor($amount / 100);
				$_second = $amount % 100;
				$this->toReturn['note_100'] = (int)$_first;
				break;
			case $amount >= 50;
				$_first = floor($amount / 50);
				$_second = $amount % 50;
				$this->toReturn['note_50'] = (int)$_first;
				break;
			case $amount >= 20;
				$_first = floor($amount / 20);
				$_second = $amount % 20;
				$this->toReturn['note_20'] = (int)$_first;
				break;
		}

		if( $_second > 0 )
		{
			return $this->_dispense($_second, true);
		}

		return $this->toReturn;
	}

	public function setSkipNote($noteType = null)
	{
		if( !in_array($noteType, $this->getAllBankNotes()) )
		{
			return false;
		}

		if( !is_numeric($amount) || $amount <= 0 )
		{
			$amount = 0;
		}

		$this->skipNote = $noteType;
	}

}