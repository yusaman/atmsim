<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ATMSettings extends Model
{
    protected $table = 'atm_settings';
    public $timestamps = true;
}
