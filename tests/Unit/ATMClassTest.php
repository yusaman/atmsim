<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use App\Classes\ATM;
use App\Accounts;
use App\ATMSettings;

class ATMClassTest extends TestCase
{
	// use RefreshDatabase;

    public function testAllATMFunction()
    {
    	$account = Accounts::first();
    	$settings = ATMSettings::first();

    	$atm = new ATM();
    	$atm->init($settings, $account);

    	$allBankNotes = $atm->getAllBankNotes();
        $this->assertEquals($allBankNotes, [20, 50, 100, 500, 1000]);

    	$getNote20 = $atm->getBankNote(20);
    	$getNote50 = $atm->getBankNote(50);
    	$getNote100 = $atm->getBankNote(100);
    	$getNote500 = $atm->getBankNote(500);
    	$getNote1000 = $atm->getBankNote(1000);

    	$this->assertEquals($getNote20, 100);
    	$this->assertEquals($getNote50, 100);
    	$this->assertEquals($getNote100, 100);
    	$this->assertEquals($getNote500, 100);
    	$this->assertEquals($getNote1000, 100);

    	$setNote20 = $atm->setBankNote(20, 20);
    	$setNote50 = $atm->setBankNote(50, 50);
    	$setNote100 = $atm->setBankNote(100, 100);
    	$setNote500 = $atm->setBankNote(500, 500);
    	$setNote1000 = $atm->setBankNote(1000, 1000);

    	$getNote20 = $atm->getBankNote(20);
    	$getNote50 = $atm->getBankNote(50);
    	$getNote100 = $atm->getBankNote(100);
    	$getNote500 = $atm->getBankNote(500);
    	$getNote1000 = $atm->getBankNote(1000);
    	
    	$this->assertEquals($getNote20, 20);
    	$this->assertEquals($getNote50, 50);
    	$this->assertEquals($getNote100, 100);
    	$this->assertEquals($getNote500, 500);
    	$this->assertEquals($getNote1000, 1000);

    	$allBankNotesWithValue = $atm->getAllBankNotesWithValue();
    	$this->assertEquals($allBankNotesWithValue['note_20'], 400);
    	$this->assertEquals($allBankNotesWithValue['note_50'], 2500);
    	$this->assertEquals($allBankNotesWithValue['note_100'], 10000);
    	$this->assertEquals($allBankNotesWithValue['note_500'], 250000);
    	$this->assertEquals($allBankNotesWithValue['note_1000'], 1000000);

    	$total = $atm->getTotalAmount();
    	$this->assertEquals($total, 1262900);

    	$atm->setBalance(92992);
    	$w = $atm->dispense(1020);
    	$this->assertTrue($w['note_1000'] == 1 );
    	$this->assertTrue($w['note_500'] == 0 );
    	$this->assertTrue($w['note_100'] == 0 );
    	$this->assertTrue($w['note_50'] == 0 );
    	$this->assertTrue($w['note_20'] == 1 );

    	$w = $atm->dispense(9890);
    	$this->assertTrue($w['note_1000'] == 9 );
    	$this->assertTrue($w['note_500'] == 1 );
    	$this->assertTrue($w['note_100'] == 3 );
    	$this->assertTrue($w['note_50'] == 1 );
    	$this->assertTrue($w['note_20'] == 2 );

    }

}
