# installation
run the following command in order
1. composer install
2. create .env file & correct DB connection info ( can copy from .env.example )
3. php artisan key:generate
4. php artisan migrate
5. php artisan db:seed
6. php artisan serve
7. browse http://localhost:8000 or http://127.0.0.1:8000