<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <style>
            .border{
                border: 1px solid #ccc;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>ATM Simulator</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 border">
                    
                    <h3>Availalble banknotes</h3>
                    <p>
                        @foreach($bankNotes as $noteType => $noteCount)
                            <strong>note {{str_replace('note_', '', $noteType)}}</strong>
                            <span class='badge badge-default'>{{$noteCount}}</span> <br>
                        @endforeach
                    </p>
                    
                    <form action="{{route('home')}}" method="POST">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">AVAILABLE: {{number_format($total)}}</h3>
                                <h4 class="card-title">YOUR BALANCE: {{number_format($balance)}}</h4>
                                <p class='card-body'>
                                    <label for="withdrawal">Withdrawal</label>
                                    <input type="text" name="withdrawal" id="withdrawal" class="form-control inline" placeholder="withdrawal" autocomplete="off">
                                    {{csrf_field()}}
                                    <button class="btn btn-primary">Withdraw</button>
                                </p>
                            </div>
                        </div>
                    </form>

                    @if(isset($toWithdraw))
                    <div class="card">
                        <div class="card-body">
                            <h3 class="card-title">RESULTS</h3>
                            @if($returnNotes !== false)
                            <p class='card-body'>
                                <strong>to withdraw: {{$toWithdraw}}</strong>
                                <p class="alert alert-success" role="alert">
                                    @foreach($returnNotes as $noteType => $noteCount)
                                        <strong>note {{str_replace('note_', '', $noteType)}}</strong>
                                        <span class='badge badge-default'>{{$noteCount}}</span> <br>
                                    @endforeach
                                </p>
                            </p>
                            
                            @else
                                <p class='card-body'>
                                    <strong>to withdraw: {{$toWithdraw}}</strong>
                                    <p class="alert alert-danger" role="alert">
                                      Can not done!
                                    </p>
                                </p>
                                
                            @endif
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-md-4 border">

                    <form action="{{route('settings.post')}}" method="POST">
                        <h2>SETTINGS</h2>
                        
                        <div class="card">
                            <div class="card-body">
                            <h3 class="card-title">Bank notes</h3>
                            <p class='card-body'>
                                <label for="note_20">$20</label>
                                <input type="text" name="bankNotes[note_20]" id="note_20" class="form-control inline" placeholder="Note 20" value="{{$bankNotes['note_20']}}" autocomplete="off"> bank note(s)
                                <br>
                                <label for="note_20">$50</label>
                                <input type="text" name="bankNotes[note_50]" id="note_50" class="form-control inline" placeholder="Note 50" " value="{{$bankNotes['note_50']}}" autocomplete="off"> bank note(s)
                                <br>
                                <label for="note_20">$100</label>
                                <input type="text" name="bankNotes[note_100]" id="note_100" class="form-control inline" placeholder="Note 100" " value="{{$bankNotes['note_100']}}" autocomplete="off"> bank note(s)
                                <br>
                                <label for="note_20">$500</label>
                                <input type="text" name="bankNotes[note_500]" id="note_500" class="form-control inline" placeholder="Note 500" " value="{{$bankNotes['note_500']}}" autocomplete="off"> bank note(s)
                                <br>
                                <label for="note_20">$1000</label>
                                <input type="text" name="bankNotes[note_1000]" id="note_1000" class="form-control" placeholder="Note 1000" " value="{{$bankNotes['note_1000']}}" autocomplete="off"> bank note(s)
                            </p>
                            <p>
                                <strong>TOTAL: </strong> {{number_format($total)}}
                            </p>
                        </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title">Balance</h3>
                                <p class='card-body'>
                                    <label for="initial_balance">Initial balance</label>
                                    <input type="text" name="initial_balance" id="initial_balance" class="form-control inline" placeholder="Initial balance" value="{{$balance}}" autocomplete="off">
                                </p>
                            </div>
                        </div>
                        {{csrf_field()}}
                        <button class="btn btn-primary">Submit</button>
                        <a href="{{route('landing')}}" class='btn btn-default'>Reset</a>
                    </form>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <h3>..........Footer..........</h3>
                </div>
            </div>
        </div>
    </body>
</html>
